# Curso - Gitlab-ci

Este curso tem por objetivo apresentar, através de um estudo de caso simples e didático, como uma estrutura de Gitlab-ci
pode ser construída para uso em projetos de desenvolvimento, integração e deploy contínuos.

![Pipeline](pipeline.png)

## Pré-Requisitos
 - Criação de conta no gitlab.com
 - Instalação do Git/Docker na máquina de desenvolvimento
 
## Sobre o Aplicação de Estudo de Caso
 - Extremamente simples
 - Fins meramente didáticos
 - Aplicação HTML estática
 
## Sobre o Fluxo de Desenvolvimento/Integração/Deploy
 - Teste da estrutura dos documentos HTML
 - Teste de interface com Selenium
 - Homologação em ambiente específico (para cada merge request)
 - Deploy na nuvem (Digital Ocean)

## Primeiros Passos
 - Clone do projeto (https://gitlab.com/brenokcc/gitlab-ci.git)
 - Compreensão da estrutura do projeto
 - Execução da aplicação (máquina local VS container)
 - Teste da aplicação (máquina local VS container)
 
## Trabalhando com Git
 - Atualização da branch master
 - Criação de novas branches
 - Commit
 - Push
 - Criação de merge requests
 
 ## Trabalhando com Docker
 - Download de imagens
 - Inicialização de containers (portas e volumes)
 - Execução de comandos em containers
 - Listagem de containers
 - Finalização de containers (stop e rm)
 - Criação de imagens (Dockerfile)
 - Publicação de imagens

## Desenvolvimento

A execução e teste da aplicação de estudo de caso requer a instalação do NodeJS.

### Instalação
 - Mac OS ("brew install node")
 - Unix ("apt install nodejs")
 
### Execução da Aplicaçao
./run.sh

### Teste da Aplicação
/test.sh

## Gitlab-ci

Requer a configuração de um único arquivo denominado "gitlab-ci.yml", localizado na raíz da aplicação.

### Arquivo .gitlab-ci.yml

``` 
stages:
  - stage_a
  - stage_b
  - stage_c
  - stage_d

job_a:
  stage: stage_a
  needs: [<job-name>]
  before_script:
    -
  script:
    -
  when: manual | always | on_failure | on_success
  only:
    - merge_requests
    - <branch-name>

``` 

### Execução do primeiro pipeline

- Crie o arquivo .gitlab-ci.yml na raíz do projeto
- Defina um único estágio chamado "teste"
- Defina uma única tarefa chamada "testar"
- Crie uma branch com seu nome
- Realize o commit e push do arquivo .gitlab-ci.yml recém criado
- Visualize a execução do pipeline no gitlab.com

### Entendendo o gitlab-runner
- Shared runner
- Private runner

## Configurando seu próprio gitlab-runner

### Instalação

wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

chmod +x /usr/local/bin/gitlab-runner

useradd --comment "GitLab Runner" --create-home gitlab-runner --shell /bin/bash

gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

rm -f /home/gitlab-runner/.bash_logout

GITLAB_URL=https://gitlab.com/

RUNNER_TOKEN=XXXXXXX

RUNNER_TAGS=default

gitlab-runner register --non-interactive --url $GITLAB_URL --registration-token $RUNNER_TOKEN --executor "shell" --description Gitlab.com --tag-list "$RUNNER_TAGS" --run-untagged="true" --locked="false"

usermod -aG docker gitlab-runner

visudo
``` 
gitlab-runner ALL=(ALL) NOPASSWD: /usr/local/bin/docker
``` 

### Inicialização

gitlab-runner start

## Criação da imagem

ci/build-image.sh

## Criação de containers

ci/create-container.sh <name>

## Remoção de containers

ci/destroy-container.sh <name>

## Publicação da Imagem

docker tag curso-gitlab-ci brenokcc/curso-gitlab-ci:0.0.1

docker push brenokcc/curso-gitlab-ci:0.0.1




### Teste de Interface

curl -L https://github.com/mozilla/geckodriver/releases/download/v0.27.0/geckodriver-v0.27.0-linux64.tar.gz --output geckodriver.tar.gz
gunzip geckodriver.tar.gz
tar -xf geckodriver.tar
mv geckodriver /usr/local/bin/
rm geckodriver.tar