set -axe
DIR=$(dirname "$BASH_SOURCE")
echo "Building image from $DIR"
docker build -t curso-gitlab-ci "$DIR"