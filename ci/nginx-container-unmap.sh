set -axe
CONTAINER_NAME=$1
rm -f "/etc/nginx/conf.d/$CONTAINER_NAME.conf"
nginx -s reload